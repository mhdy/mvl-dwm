/* See LICENSE file for copyright and license details. */

/* options */
static const unsigned int borderpx = 1;  /* border pixel of windows */
static const unsigned int snap     = 32; /* snap pixel */
static const unsigned int gappx    = 10;  /* gaps between windows */
static const int showbar           = 1;  /* 0 means no bar */
static const int topbar            = 1;  /* 0 means bottom bar */
static const char *fonts[]         = {
	"Roboto:size=11",
  "Material Design Icons:size=12",
};
static const char dmenufont[] = "Roboto:size=11";
static const char *dmenucmd[] = { "dmenu_run", "-p", "> ", NULL };

/* theme */
#include "themes/ghyam.h"
static const char *colors[][3] = {
  [SchemeNorm] = { col_norm_fg, col_norm_bg, col_norm_border },
  [SchemeSel]  = { col_sel_fg, col_sel_bg, col_sel_border },
};

/* layout(s) */
static const float mfact     = 0.5;  /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol, arrange function */
	{ "[T]", tile }, /* first entry is default */
	{ "[M]", monocle },
	{ "[F]", NULL }, /* no layout function means floating behavior */
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "vm", "email", "web" };

/* rules */
static const Rule rules[] = {
  /* class, instance, title, tags mask, isfloating, monitor */
  { "Firefox", NULL, NULL, 1 << 8, 0, -1 },
  { "Mmail", NULL, NULL, 1 << 7, 0, -1 },
  { "Virt-manager", NULL, NULL, 1 << 6, 0, -1 },
  { "Nsxiv", NULL, NULL, 0, 1, -1 },
  { "Pulsemixer", NULL, NULL, 0, 1, -1 },
  { "Calcurse", NULL, NULL, 0, 1, -1 },
  { "Float", NULL, NULL, 0, 1, -1 },
};

/* commands */
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { "openscratchpad", NULL };

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* key bindings */
static Key keys[] = {
  /* modifier             key           function       argument */
  /* layouts */
  { MODKEY|Mod1Mask,      XK_t,         setlayout,     { .v = &layouts[0]} },
  { MODKEY|Mod1Mask,      XK_m,         setlayout,     { .v = &layouts[1]} },
  { MODKEY|Mod1Mask,      XK_f,         setlayout,     { .v = &layouts[2]} },
  /* views and tags */
  { MODKEY,               XK_0,         view,          { .ui = ~0 } },
  { MODKEY|ShiftMask,     XK_0,         tag,           { .ui = ~0 } },
  TAGKEYS(                XK_1,                        0)
  TAGKEYS(                XK_2,                        1)
  TAGKEYS(                XK_3,                        2)
  TAGKEYS(                XK_4,                        3)
  TAGKEYS(                XK_5,                        4)
  TAGKEYS(                XK_6,                        5)
  TAGKEYS(                XK_7,                        6)
  TAGKEYS(                XK_8,                        7)
  TAGKEYS(                XK_9,                        8)
  /* ui */
  { MODKEY,               XK_d,         spawn,         { .v = dmenucmd } },
  { MODKEY,               XK_b,         togglebar,     {0} },
  { MODKEY,               XK_k,         focusstack,    { .i = -1 } },
  { MODKEY,               XK_j,         focusstack,    { .i = +1 } },
  { MODKEY|ShiftMask,     XK_equal,     incnmaster,    { .i = +1 } },
  { MODKEY,               XK_minus,     incnmaster,    { .i = -1 } },
  { MODKEY,               XK_h,         setmfact,      { .f = -0.05 } },
  { MODKEY,               XK_l,         setmfact,      { .f = +0.05 } },
  { MODKEY,               XK_backslash, zoom,          { 0 } },
  { MODKEY,               XK_Tab,       view,          { 0 } },
  { MODKEY,               XK_q,         killclient,    { 0 } },
  { MODKEY,               XK_End,       quit,          { 0 } },
  /* monitors */
  { MODKEY,               XK_comma,     focusmon,      { .i = -1 } },
  { MODKEY,               XK_period,    focusmon,      { .i = +1 } },
  { MODKEY|ShiftMask,     XK_comma,     tagmon,        { .i = -1 } },
  { MODKEY|ShiftMask,     XK_period,    tagmon,        { .i = +1 } },
  /* commands */
	{ MODKEY|ShiftMask,     XK_Return,    togglescratch, { .v = scratchpadcmd } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
  /* click,       mask,             button,  function,       argument */
  { ClkTagBar,    0,                Button1, view,           {0} },
  { ClkTagBar,    ControlMask,      Button1, toggleview,     {0} },
  { ClkTagBar,    MODKEY|ShiftMask, Button1, tag,            {0} },
  { ClkLtSymbol,  0,                Button1, setlayout,      {0} },
  { ClkClientWin, MODKEY,           Button1, movemouse,      {0} },
  { ClkClientWin, MODKEY,           Button2, togglefloating, {0} },
  { ClkClientWin, MODKEY,           Button3, resizemouse,    {0} },
};
