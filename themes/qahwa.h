/* qahwa */
static const char col_norm_bg[] = "#212121";
static const char col_norm_fg[] = "#bdbdbd";
static const char col_sel_bg[] = "#4e342e";
static const char col_sel_fg[] = "#ffffff";
static const char col_norm_border[] = "#424242";
static const char col_sel_border[] = "#4e342e";
