# dwm

This repository contains the [dwm](http://dwm.suckless.org/) window manager I use.

All applied patches can be found in the `patches` directory.
